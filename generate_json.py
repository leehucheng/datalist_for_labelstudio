import os
import glob
from tqdm import tqdm
import json
import os 
import argparse
import urllib.parse
from pathlib import Path
import random
SEED = 2700


def save_json(json_filename, meta):
    json_dir = os.path.dirname(json_filename)
    Path(json_dir).mkdir(parents=True, exist_ok=True)
    with open(json_filename, 'w') as f:
        json.dump(meta, f)

def parse_videopath(args, mode='dir'):
    videos = []
    if mode == 'dir':
        for d_dir in args.data_dir:
            videos.extend(sorted(glob.glob(os.path.join(d_dir, '*.{}'.format(args.extension)))))
    elif mode == 'json':
        with open(args.json_input) as f:
            videos.extend(json.load(f))
    elif mode == 'dir-sample':
        for d_dir in args.data_dir:
            tmp = sorted(glob.glob(os.path.join(d_dir, '*.{}'.format(args.extension))))
            random.Random(SEED).shuffle(tmp)
            sample_num = len(tmp) if args.sample_num > len(tmp) else args.sample_num
            videos.extend(tmp[:sample_num])

    return videos

def generate_videopath(videos, args):
    meta = []
    for video in tqdm(videos):
        video_basename = os.path.basename(video)
        tmp = {}
        tmp["data"] = {}
        if args.extension == 'mp4':
            if args.local == False:
                tmp["data"]["video"] = "<video src='http://{}:{}/{}'".format(args.ip_address, args.port, video_basename)
                if args.version == 'old':
                    tmp["data"]["video"] += " width=100% muted /><img src onerror=\"$=n=>document.getElementsByTagName(n)[0];a=$('audio');v=$('video');a.onseeked=()=>{v.currentTime=a.currentTime};a.onplay=()=>v.play();a.onpause=()=>v.pause()\" />"
                elif args.version == 'new':
                    tmp["data"]["video"] += " width=100% muted/><img src onerror=\"$=n=>document.querySelector(n);a=$('.ls-editor audio');v=$('video');a.onseeked=()=>{v.currentTime=a.currentTime};a.onplay=()=>v.play();a.onpause=()=>v.pause()\" />"
                tmp["data"]["videoSource"] = "http://{}:{}/{}".format(args.ip_address, args.port, video_basename)
            elif args.local == True:
                if args.version == 'old':
                    filename = os.path.basename(video)
                    image_url_path = urllib.parse.quote('data/' + filename)
                    params = urllib.parse.urlencode({'d': os.path.dirname(video)})
                    tmp["data"]["video"] = "<video src='{}?{}'".format(image_url_path, params)
                    tmp["data"]["video"] += " width=100% muted /><img src onerror=\"$=n=>document.getElementsByTagName(n)[0];a=$('audio');v=$('video');a.onseeked=()=>{v.currentTime=a.currentTime};a.onplay=()=>v.play();a.onpause=()=>v.pause()\" />"
                    tmp["data"]["videoSource"] = "{}?{}".format(image_url_path, params)
                elif args.version == 'new':
                    tmp["data"]["video"] = "<video src='data/local-files?d={}'".format(video)
                    tmp["data"]["video"] += " width=100% muted/><img src onerror=\"$=n=>document.querySelector(n);a=$('.ls-editor audio');v=$('video');a.onseeked=()=>{v.currentTime=a.currentTime};a.onplay=()=>v.play();a.onpause=()=>v.pause()\" />"
                    tmp["data"]["videoSource"] = "data/local-files?d={}".format(video)
        elif args.extension == 'wav':
            if args.local == False:
                tmp["data"]["audio"] = "http://{}:{}/{}".format(args.ip_address, args.port, video_basename)
            elif args.local == True:
                if args.version == 'old':
                    filename = os.path.basename(video)
                    image_url_path = urllib.parse.quote('data/' + filename)
                    params = urllib.parse.urlencode({'d': os.path.dirname(video)})
                    tmp["data"]["audio"] = "{}?{}".format(image_url_path, params)
                    tmp["data"]["task_path"] = video
                elif args.version == 'new':
                    tmp["data"]["audio"] = "data/local-files?d={}".format(video)
        meta.append(tmp)
    return meta

def main(args):
    videos = parse_videopath(args, mode=args.mode)
    meta = generate_videopath(videos, args)
    save_json(args.json_filename, meta)
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='generate json file for label studio')
    parser.add_argument('-m', '--mode', default='dir', choices=['dir','json','dir-sample'])
    parser.add_argument('-d', '--data_dir', default=['/KIKI/hucheng/audio/2021-06-01/NoEvent'], nargs='+', help='the directories of data')
    parser.add_argument('-j', '--json_input', default='input.json',type=str, help='input videopaths')
    parser.add_argument('-f', '--json_filename', default='task.json', type=str, help='the filename of json file')
    parser.add_argument('-s', '--sample_num', default=100, type=int, help='sample number of files in a directory')
    parser.add_argument('-p', '--port', default=3000, type = int, help='the port of local file uploading')
    parser.add_argument('-i', '--ip_address', default='172.17.0.127', type = str, help='the ip address of local file uploading')
    parser.add_argument('-e', '--extension', default='wav', type= str, choices=['mp4','wav'], help='the extension of files you want to generate')
    parser.add_argument('-v', '--version', default='old', type = str, choices= ['old','new'], help='the version before/after v1.0.0')
    parser.add_argument('-l', '--local', default=True, type = bool, help='Whether to use local prefix')
    args = parser.parse_args()

    main(args)