# DataList_for_LabelStudio #

This README would document how to generate data list for label-studio input. There're three modes of generation - dir, json, dir-sample.

### argument explanation ###
```mode``` - generation mode of json file. We can select ```'dir'```, ```'json'```, ```'dir-sample'```.

```extension``` - the extension of target files. We can select ```'wav'```, ```'mp4'```.

```version``` - label-studio version. If the version is before v1.0.0, select ```old```. Otherwise, select ```new```.

```local``` - whether to use local prefix. If so, select ```True```. Otherwise, select ```False```, and we have to specify ```port``` and ```ip_address```, and then we have to launch http-server.

## dir mode ##
We can select multiple directories, glob all the specific extension files in those directories, and output a json file for label-studio. 

### example usage ###
```bash
python generate_json.py --mode dir --data_dir [directories_of_target_files] --json_filename [output_path_of_jsonfile] --extension wav --version old --local True
```

If you want to generate list of the data (wav) which are stored in ```/KIKI/hucheng/audio/2021-06-01/NoEvent``` and output json file as ```Furbo_NoEvent_20210601.json```, you can execute the following command:

```bash
python generate_json.py --mode dir --data_dir /KIKI/hucheng/audio/2021-06-01/NoEvent --json_filename Furbo_NoEvent_20210601.json --extension wav --version old --local True
```

## json mode ##
We can input a json file which is saved as list and output a json file for label-studio. 

### example usage ###
```bash
python generate_json.py --mode json --json_input [path_of_input_json] --json_filename [output_path_of_jsonfile] --extension wav --version old --local True
```

If you want to generate list of the data (wav) which are stored as list in ```input.json``` and output json file as ```Furbo_NoEvent_20210601.json```, you can execute the following command:

```bash
python generate_json.py --mode dir --json_input input.json --json_filename Furbo_NoEvent_20210601.json --extension wav --version old --local True
```

## dir-sample mode ##
We can select multiple directories, randomly sample some data with the specific extension files in those directories and output a json file for label-studio. 

### example usage ###
```bash
python generate_json.py --mode dir-sample --data_dir [directories_of_target_files] --sample_num [number_of_sample_data] --json_filename [output_path_of_jsonfile] --extension wav --version old --local True
```

If you want to generate list of the data (wav) which are stored in ```/KIKI/hucheng/audio/2021-06-01/NoEvent```, sample 100 data in those directories, and output json file as ```Furbo_NoEvent_20210601.json```, you can execute the following command:

```bash
python generate_json.py --mode dir --data_dir /KIKI/hucheng/audio/2021-06-01/NoEvent --sample_num 100 --json_filename Furbo_NoEvent_20210601.json --extension wav --version old --local True
```

